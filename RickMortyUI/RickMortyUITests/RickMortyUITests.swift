import XCTest
@testable import RickMortyUI
import Alamofire
import Foundation

final class RickMortyUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFilterCharactersByGender() {
            let characters = [
                Character(id: 1, name: "Rick", status: "Alive", species: "Human", type: "Scientist", gender: "Male", origin: Location(name: "Earth", url: "earth-url"), location: Location(name: "Earth", url: "earth-url"), image: "rick-image", episode: ["episode-1", "episode-2"], url: "rick-url", created: "creation-date"),
                Character(id: 2, name: "Morty", status: "Alive", species: "Human", type: "Grandson", gender: "Male", origin: Location(name: "Earth", url: "earth-url"), location: Location(name: "Earth", url: "earth-url"), image: "morty-image", episode: ["episode-1", "episode-2"], url: "morty-url", created: "creation-date"),
                Character(id: 3, name: "Summer", status: "Alive", species: "Alien", type: nil, gender: "Female", origin: Location(name: "Other Planet", url: "planet-url"), location: Location(name: "Earth", url: "earth-url"), image: "summer-image", episode: ["episode-3", "episode-4"], url: "summer-url", created: "creation-date")
            ]

            let characterList = CharacterList(info: Info(count: 3, pages: 1, next: nil, prev: nil), results: characters)

            let filteredCharacters = characterList.filterCharacters(byGender: "Male")

            XCTAssertEqual(filteredCharacters.count, 2)
            XCTAssertTrue(filteredCharacters.allSatisfy { $0.gender == "Male" })
        }
    
    func testFetchCharacters() {
        let viewModel = CharactersViewModel()

        let requestExpectation = expectation(description: "Network Request")
    
        AF.request("https://rickandmortyapi.com/api/character").responseDecodable(of: CharacterList.self) { response in
            DispatchQueue.main.async {
                switch response.result {
                case .success(let characterList):
                    viewModel.characters = characterList.results
                    XCTAssertTrue(!viewModel.characters.isEmpty)
                case .failure(let error):
                    XCTFail("Request failed with error: \(error)")
                }
                requestExpectation.fulfill()
            }
        }

        waitForExpectations(timeout: 5)
    }
}

protocol NetworkSession {
    func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask
}

class MockSession: NetworkSession {
    private var data: Data?
    private var response: URLResponse?
    private var error: Error?

    init(data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
    }

    func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask {
        let task = MockURLSessionDataTask()
        task.completionHandler = completionHandler
        DispatchQueue.global().async {
            task.completionHandler!(self.data, self.response, self.error)
        }
        return task
    }
}

class MockURLSessionDataTask: URLSessionDataTask {
    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?

    override func resume() {
        // This method is called when the data task is resumed, but we're simulating it by invoking the completion handler directly.
        completionHandler?(nil, nil, nil)
    }
}
