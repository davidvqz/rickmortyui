//
//  RickMortyUIUITests.swift
//  RickMortyUIUITests
//
//  Created by David Vázquez on 23/10/23.
//

import XCTest

final class RickMortyUIUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    func testCharacterList() {
        print(app.debugDescription)

        let characterList = app.collectionViews["RickMortyList"]
        XCTAssertTrue(characterList.exists)

        let firstCharacter = characterList.cells.element(boundBy: 0)
        XCTAssertTrue(firstCharacter.exists)

        firstCharacter.tap()

        let characterDetail = app.staticTexts["Rick Sanchez"]
        XCTAssertTrue(characterDetail.exists)

        app.navigationBars.buttons["Back"].tap()
    }
    
    func testNextAndPrevButtons() throws {
        
        let app = XCUIApplication()
        let ttgc7swiftui19uihostingNavigationBar = app.navigationBars["_TtGC7SwiftUI19UIHosting"]
        let nextButton = ttgc7swiftui19uihostingNavigationBar/*@START_MENU_TOKEN@*/.buttons["Next"]/*[[".otherElements[\"Next\"].buttons[\"Next\"]",".buttons[\"Next\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        nextButton.tap()
        nextButton.tap()
        nextButton.tap()
        nextButton.tap()
        
        let rickmortylistCollectionView = app.collectionViews["RickMortyList"]
        rickmortylistCollectionView.buttons.firstMatch.tap()
        let crocubot = app.staticTexts["Crocubot"]
        XCTAssertFalse(!crocubot.exists)

        let backButton = ttgc7swiftui19uihostingNavigationBar.buttons["Back"]
        backButton.tap()
        
        let prevButton = ttgc7swiftui19uihostingNavigationBar/*@START_MENU_TOKEN@*/.buttons["Prev"]/*[[".otherElements[\"Prev\"].buttons[\"Prev\"]",".buttons[\"Prev\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        prevButton.tap()
        prevButton.tap()
        prevButton.tap()
        prevButton.tap()

        rickmortylistCollectionView.buttons.firstMatch.tap()
        let rick = app.staticTexts["Rick Sanchez"]
        XCTAssertFalse(!rick.exists)
        backButton.tap()
    }
    
    func testSearch() throws {
        app.textFields["Search ..."].tap()
        
        app.collectionViews.textFields["Search ..."].tap()
        
        app.collectionViews.textFields["Search ..."].typeText("Morty")
        
        app.navigationBars["Filtros"]/*@START_MENU_TOKEN@*/.buttons["Aplicar"]/*[[".otherElements[\"Aplicar\"].buttons[\"Aplicar\"]",".buttons[\"Aplicar\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

        let rickmortylistCollectionView = app.collectionViews["RickMortyList"]
        let firstCharacter = rickmortylistCollectionView.cells.element(boundBy: 0)

        firstCharacter.tap()
        let morty = app.staticTexts["Morty Smith"]
        XCTAssertFalse(!morty.exists)

        let backButton = app.navigationBars["_TtGC7SwiftUI19UIHosting"].buttons["Back"]
        backButton.tap()
        
        app.buttons["x.circle.fill"].tap()
        
        let firstCharacterClean = rickmortylistCollectionView.cells.element(boundBy: 0)
        firstCharacterClean.tap()
        let rick = app.staticTexts["Rick Sanchez"]
        XCTAssertFalse(!rick.exists)

        backButton.tap()
    }
}
