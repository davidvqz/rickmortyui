import Foundation
enum ErrorHandler : String{
    case DefaultError = "Error"
    case ServerError = "404 Error"
    case NetworkError = "500 Error"
    case None
}
