//
//  SplashScreenView.swift
//  RickMortyUI
//
//  Created by David Vázquez on 25/10/23.
//

import SwiftUI

struct SplashScreenView: View {
    var body: some View {
        Image("splashImage")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .padding()
    }
}

struct SplashScreenView_Previews: PreviewProvider {
    static var previews: some View {
        SplashScreenView()
    }
}
