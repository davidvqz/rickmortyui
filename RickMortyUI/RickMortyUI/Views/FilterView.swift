import SwiftUI

struct FilterView: View {
    @Binding var isFilterModalPresented: Bool
    @Binding var selectedStatus: Status
    @Binding var selectedGender: Gender
    @Binding var searchText: String
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Búsqueda")) {
                    SearchBar(text: $searchText)
                }
                Section(header: Text("Filtros")) {
                    Picker("Status", selection: $selectedStatus) {
                        ForEach(Status.allCases, id: \.self) { status in
                            Text(status.rawValue).tag(status)
                        }
                    }
                    Picker("Gender", selection: $selectedGender) {
                        ForEach(Gender.allCases, id: \.self) { gender in
                            Text(gender.rawValue).tag(gender)
                        }
                    }
                }
            }
            .navigationBarTitle("Filtros")
            .navigationBarItems(trailing: Button("Aplicar") {
                isFilterModalPresented = false
            })
        }
    }
}
struct FilterView_Previews: PreviewProvider {
    @State static var isFilterModalPresented = true
    @State static var selectedStatus = Status.All
    @State static var selectedGender = Gender.All
    @State static var searchText = ""

    static var previews: some View {
        FilterView(isFilterModalPresented: $isFilterModalPresented, selectedStatus: $selectedStatus, selectedGender: $selectedGender, searchText: $searchText)
    }
}
