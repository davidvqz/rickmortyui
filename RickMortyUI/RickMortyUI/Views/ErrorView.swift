import SwiftUI

struct ErrorView: View {
    var errorType: ErrorHandler
    @State var showError: Bool = false

    var body: some View {
        VStack(spacing: 8) {
            
            Image("rickError")
                .resizable()
                .aspectRatio(contentMode: .fit)
            
            Text("It's a joke, only is a \(errorType.rawValue)")
                .fontWeight(.regular)
                .font(.title2)
            
            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(errorType: .ServerError, showError: true)
    }
}
