import SwiftUI
import Alamofire

enum Status: String, CaseIterable, Identifiable {
    var id: Status { self }

    case All, Dead, Alive, Unknown
}
enum Gender: String, CaseIterable, Identifiable {
    var id: Gender { self }
    case All, Female, Male, Genderless, Unknown
}

struct ContentView: View {
    @ObservedObject var viewModel = CharactersViewModel()
    var url = URL(string: "https://rickandmortyapi.com/api/character?page=1")!
    @State var searchUrl = URL(string: "https://rickandmortyapi.com/api/character")!
    @State private var searchText: String = ""
    @State private var isFilterViewPresented = false
    @State private var selectedStatus : Status = .All
    @State private var selectedGender : Gender = .All
    @State private var searchText2: String = ""
    @State private var search = false
    @State var char = "?"
    @State private var title = "Page 1"
    @State private var reloadCharacters : Bool? = false
    @State private var nameSearch = ""
    @State private var cleanFilter = true
    @State private var showDetail = false
    @State private var isShowingError = false

    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    if !search {
                        SearchBar(text: $searchText2)
                            .padding(8)
                            .onTapGesture {
                                searchUrl = URL(string: "https://rickandmortyapi.com/api/character")!
                                isFilterViewPresented = true
                                cleanFilter = false
                            }
                            .disabled(!cleanFilter)
                    } else {
                        HStack {
                            ZStack{
                                Text(searchText2)
                                    .padding()
                            }
                            Spacer()

                            Button(action: {
                                searchText = ""
                                searchText2 = ""
                                searchUrl = URL(string: "https://rickandmortyapi.com/api/character")!
                                char = "?"
                                viewModel.fetchCharacters(url: url)
                                cleanFilter = true
                                search = false
                            }) {
                                    Image(systemName: "x.circle.fill")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .foregroundStyle(.gray)
                            }
                            .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 32))
                        
                        }
                    }
                }

                if viewModel.characters.isEmpty && search {
                    ErrorView(errorType: viewModel.error, showError: true)
                } else {
                    List(viewModel.characters) { character in
                        NavigationLink(destination: CharacterDetail(character: character)) {
                            URLImageView(url: URL(string: character.image))
                                .frame(width: 100, height: 100)
                                .cornerRadius(50)
                            Text(character.name)
                        }.onAppear {
                            self.showDetail = true
                        }
                    }
                    .accessibilityIdentifier("RickMortyList")
                }
            }
            .onAppear() {
                if !search && !showDetail {
                    viewModel.fetchCharacters(url: url)
                } else {
                    self.showDetail = false
                }
            }
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text(title)
                        .font(.headline)
                }
                ToolbarItem(placement: .navigationBarLeading) {
                    if viewModel.showPrevButton {
                        Button("Prev") {
                            viewModel.fetchCharacters(url: viewModel.prevPage ?? url)
                        }
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    if viewModel.showNextButton {
                        Button("Next") {
                            viewModel.fetchCharacters(url: viewModel.nextPage ?? url)
                        }
                    }
                }
            }
        } .sheet(isPresented: $isFilterViewPresented) {
            FilterView(isFilterModalPresented: $isFilterViewPresented,
                       selectedStatus: $selectedStatus,
                       selectedGender: $selectedGender,
                       searchText: $searchText)
            
        }

        .onChange(of: searchText) { newValue in
            nameSearch = newValue
        }
        .onChange(of: selectedGender) { gender in
            search = true
        }
        .onChange(of: selectedStatus) { status in
            search = true
        }
        .onChange(of: isFilterViewPresented) { newValue in
            if !isFilterViewPresented {
                search = true

                if !nameSearch.isEmpty {
                    searchUrl = URL(string:"\(searchUrl)\(char)name=\(nameSearch)") ?? searchUrl
                    char = "&"
                }
                if selectedGender != .All {
                    searchUrl = URL(string:"\(searchUrl)\(char)gender=\(selectedGender)") ?? searchUrl
                    char = "&"
                }
                if selectedStatus != .All {
                    searchUrl = URL(string:"\(searchUrl)\(char)status=\(selectedStatus)") ?? searchUrl
                    char = "&"
                }
                viewModel.fetchCharacters(url: searchUrl)
                
                showFiltered(name: nameSearch, status: selectedStatus, gender: selectedGender)
            } else {
                selectedGender = .All
                selectedStatus = .All
            }
        }
        .onChange(of: viewModel.page) { newValue in
            if !search {
                title = "Page \(viewModel.page)"
            } else {
                title = ""
            }
        }
    }
    
    func showFiltered(name: String, status: Status, gender: Gender) {
        searchText2 = ""

        if !name.isEmpty {
            searchText2 = "\(searchText2)Name: \(searchText)\n"
        }
        if status != .All {
            searchText2 = "\(searchText2)Status: \(status.rawValue)\n"
        }
        if gender != .All {
            searchText2 = "\(searchText2)Gender: \(gender.rawValue)\n"
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct URLImageView: View {
    let url: URL?
    
    @State private var image: Image = Image(systemName: "photo")
    
    var body: some View {
        if let url = url {
            GeometryReader { geometry in
                image
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    .clipped()
                    .onAppear {
                        downloadImage(from: url)
                    }
            }
        } else {
            image
                .scaledToFit()
        }
    }
    
    private func downloadImage(from url: URL) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data, let uiImage = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image = Image(uiImage: uiImage)
                }
            }
        }.resume()
    }
}
