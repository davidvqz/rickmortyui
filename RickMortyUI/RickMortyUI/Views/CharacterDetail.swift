import SwiftUI

struct CharacterDetail: View {
    let character: Character
    @State private var downloadedImage: UIImage?
    @State var eps = [String]()

    var body: some View {
        VStack {
            Text("\(character.name)")
                .font(.title)
                .padding()
            Rectangle()
                .fill(.gray)
                .frame(width: 150, height: 2)
            if let downloadedImage = downloadedImage {
                Image(uiImage: downloadedImage)
                    .resizable()
                    .frame(width: 250, height: 250)
                    .clipShape(Circle())
                    .foregroundColor(.blue)
                    .padding()
            } else {
                Text("Loading image...")
            }
            VStack(alignment: .leading, spacing: 8){
                Text("Status: \(character.status)")
                Text("Specie: \(character.species)")
                Text("Type: \(character.type ?? "Unknown")")
                Text("Gender: \(character.gender)")
                Text("Origin: \(character.origin.name)")
                Text("Location: \(character.location.name)")
                Text("Episodes: \(eps.joined(separator: ", "))")
            }
            .padding(30)
            Spacer()
            
        }.accessibilityIdentifier("characterDetail")
        .onAppear {
            downloadImage()
            checkEpisodes(character: character)
        }
    }

    func checkEpisodes(character: Character) {
        for ep in character.episode {
            eps.append(ep.components(separatedBy: "/").last!)
        }
    }
    
    private func downloadImage() {
        print("IMAGEN \(character.image)")
        let task = URLSession.shared.dataTask(with: URL(string: character.image)!) { (data, response, error) in
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    downloadedImage = image
                }
            }
        }
        task.resume()
    }
}

struct CharacterDetail_Preview: PreviewProvider {
    static var previews: some View {
        // Crea una instancia de tu modelo Character con datos de ejemplo
        let sampleCharacter = Character(
            id: 1,
            name: "Nombre",
            status: "Sample Character",
            species: "Alive",
            type: "Human",
            gender: "Main Character",
            origin: Location(name: "Origin", url: ""),
            location: Location(name: "Origin", url: ""),
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            episode: ["1"], // Puedes proporcionar las URL de los episodios
            url: "1",
            created: ""
        )

        return CharacterDetail(character: sampleCharacter)
    }
}
