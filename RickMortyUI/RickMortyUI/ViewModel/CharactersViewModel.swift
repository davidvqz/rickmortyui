import Foundation
import Alamofire

class CharactersViewModel: ObservableObject {
    @Published var characters = [Character]()
    @Published var error : ErrorHandler = .None
    var page = "1"
    var nextPage = URL(string: "")
    var prevPage = URL(string: "")
    var url = URL(string: "https://rickandmortyapi.com/api/character?page=1")
    var showPrevButton = false
    var showNextButton = false
    
    func fetchCharacters(url: URL) {
        print(url)
        AF.request(url).responseDecodable(of: CharacterList.self) { response in
            switch response.result {
            case .success:
                do {
                    let decoder = JSONDecoder()
                    if let data = response.data {
                        let characterList = try decoder.decode(CharacterList.self, from: data)
                        DispatchQueue.main.async {
                            self.characters = characterList.results
                            
                            self.nextPage = URL(string: characterList.info.next ?? "")
                            self.prevPage = URL(string: characterList.info.prev ?? "")
                            
                            self.getPage(url: url)
                        }
                    }
                } catch {
                    self.characters.removeAll()
                    self.showPrevButton = false
                    print("Error decoding JSON: \(error)")
                }
                
            case .failure(let error):
                self.characters.removeAll()
                self.showNextButton = false
                self.showPrevButton = false
                switch response.response?.statusCode {
                case 404:
                    self.error = .ServerError
                case 500:
                    self.error = .NetworkError
                default:
                    self.error = .DefaultError
                }
                print("Request failed with error: \(error)")
            }
        }
    }
    
    func getPage(url: URL) {
        self.page = url.absoluteString.components(separatedBy: "=").last ?? "1"

        if self.nextPage == nil {
            self.showNextButton = false
        }
        if self.prevPage == nil {
            self.showPrevButton = false
        }
        if self.nextPage != nil {
            self.showNextButton = true
        }
        if self.prevPage != nil {
            self.showPrevButton = true
        }
    }
    
}

